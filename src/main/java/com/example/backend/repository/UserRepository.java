package com.example.backend.repository;

import com.example.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findByEmailAndPassword(String email, String password);

    @Query("SELECT u FROM User u WHERE u.name LIKE CONCAT('%',:query, '%')"
            + " OR u.surname LIKE CONCAT('%',:query, '%')"
            + " OR u.email LIKE CONCAT('%',:query, '%')"
            + " OR u.password LIKE CONCAT('%',:query, '%')")
    List<User> searchAll(String query);
}
