package com.example.backend.repository;

import com.example.backend.model.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NumberRepository extends JpaRepository<PhoneNumber, Long> {
}
