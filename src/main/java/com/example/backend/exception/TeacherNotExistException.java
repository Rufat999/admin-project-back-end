package com.example.backend.exception;

public class TeacherNotExistException extends RuntimeException{
    public TeacherNotExistException(String message){
        super(message);
    }
}
