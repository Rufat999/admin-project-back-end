package com.example.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(UserNotExistException.class)
    public ResponseEntity<?> userNotExistException(UserNotExistException userNotExistException){
        userNotExistException.printStackTrace();
        return new ResponseEntity<>(userNotExistException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotNullException.class)
    public ResponseEntity<?> notNullException(NotNullException notNullException){
        notNullException.printStackTrace();
        return new ResponseEntity<>(notNullException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PasswordMatchException.class)
    public ResponseEntity<?> bookReleaseDateException(PasswordMatchException passwordMatchException){
        passwordMatchException.printStackTrace();
        return new ResponseEntity<>(passwordMatchException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    public ResponseEntity<?> emailAlreadyExistsException(EmailAlreadyExistsException emailAlreadyExistsException){
        emailAlreadyExistsException.printStackTrace();
        return new ResponseEntity<>(emailAlreadyExistsException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TeacherNotExistException.class)
    public ResponseEntity<?> teacherNotExistException(TeacherNotExistException teacherNotExistException){
        teacherNotExistException.printStackTrace();
        return new ResponseEntity<>(teacherNotExistException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CommentNotExistException.class)
    public ResponseEntity<?> commentNotExistException(CommentNotExistException commentNotExistException){
        commentNotExistException.printStackTrace();
        return new ResponseEntity<>(commentNotExistException.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
