package com.example.backend.service;

import com.example.backend.dto.CommentRequest;
import com.example.backend.dto.CommentResponse;
import com.example.backend.exception.CommentNotExistException;
import com.example.backend.exception.TeacherNotExistException;
import com.example.backend.model.Comment;
import com.example.backend.model.Teacher;
import com.example.backend.repository.CommentRepository;
import com.example.backend.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final TeacherRepository teacherRepository;
    private final ModelMapper modelMapper;

    public List<CommentResponse> getAllComments() {
        List<Comment> allComments = commentRepository.findAll();
        List<CommentResponse> commentResponseList = allComments.stream().map(comment -> modelMapper.map(comment, CommentResponse.class)).collect(Collectors.toList());
        return commentResponseList;
    }

    public CommentResponse add(Long teacherId, CommentRequest request) {
        Teacher teacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new TeacherNotExistException("Teacher " + teacherId + " not found!"));
        Comment comment = modelMapper.map(request, Comment.class);
        teacher.getComments().add(comment);
        comment.setTeacher(teacher);

        commentRepository.save(comment);

        CommentResponse commentResponse = modelMapper.map(comment, CommentResponse.class);

        return commentResponse;
    }

    public CommentResponse update(Long commentId, CommentRequest request) {
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new CommentNotExistException("Comment " + commentId + " not found!"));
        comment.setComment(request.getComment());
        commentRepository.save(comment);
        CommentResponse commentResponse = modelMapper.map(comment, CommentResponse.class);
        return commentResponse;
    }

    public void deleteById(Long commentId) {
        commentRepository.deleteById(commentId);
    }

    public void deleteAll() {
        commentRepository.deleteAll();
    }
}
