package com.example.backend.service;

import com.example.backend.dto.*;
import com.example.backend.exception.EmailAlreadyExistsException;
import com.example.backend.exception.NotNullException;
import com.example.backend.exception.UserNotExistException;
import com.example.backend.model.User;
import com.example.backend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserResponse getUserById(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User " + userId + " not found!"));
        UserResponse userResponse = modelMapper.map(user, UserResponse.class);
        return userResponse;
    }

    public List<UserResponse> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserResponse> userResponseList = users.stream().map(user -> modelMapper.map(user, UserResponse.class)).collect(Collectors.toList());
        return userResponseList;
    }

    public UserResponse registerUser(UserRequest request) {
        Optional<User> userExistsWithThatEmail = userRepository.findByEmail(request.getEmail());
        User user = modelMapper.map(request, User.class);
        if (Objects.equals(request.getSurname(), "") || Objects.equals(request.getName(), "") ||
                Objects.equals(request.getEmail(), "") || Objects.equals(request.getPassword(), "")) {
            throw new NotNullException("Enter something");
        } else if (userExistsWithThatEmail.isPresent()) {
            throw new EmailAlreadyExistsException("Email Already Exists!");
        } else {
            userRepository.save(user);
        }
        UserResponse userResponse = modelMapper.map(user, UserResponse.class);
        return userResponse;
    }


    public UserResponse resetPassword(UserResetPasswordRequest request) {
        Optional<User> userExistsInDatabase = userRepository.findByEmailAndPassword(request.getEmail(), request.getPassword());

        if (userExistsInDatabase.isPresent() && !Objects.equals(request.getPassword(), "") && !Objects.equals(request.getNewPassword(), "")) {
            userExistsInDatabase.map(user -> {
                user.setPassword(request.getNewPassword());
                return userRepository.save(user);
            });
        } else if (userExistsInDatabase.isEmpty()) {
            throw new UserNotExistException("User Not Exists!");
        } else if (Objects.equals(request.getPassword(), "")) {
            throw new NotNullException("Enter something password!");
        } else if (Objects.equals(request.getNewPassword(), "")) {
            throw new NotNullException("Enter something newPassword!");
        }

        UserResponse userResponse = modelMapper.map(userExistsInDatabase, UserResponse.class);
        return userResponse;
    }

    public void deleteUserById(Long userId) {
        userRepository.deleteById(userId);
    }

    public void deleteAllUsers() {
        userRepository.deleteAll();
    }

    public String login(UserLoginRequest request) {
        Optional<User> emailAndPasswordExistsInDatabase = userRepository.findByEmailAndPassword(request.getEmail(), request.getPassword());
        if (emailAndPasswordExistsInDatabase.isPresent()) {
            return "Successfully Complete";
        } else if (Objects.equals(request.getEmail(), "") || Objects.equals(request.getPassword(), "")) {
            throw new NotNullException("Enter something");
        } else {
            throw new UserNotExistException("Email And Password Not True");
        }
    }

    public UserResponse update(Long userId, UserRequest request) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotExistException("User " + userId + " not found!"));
        user.setName(request.getName());
        user.setSurname(request.getSurname());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());

        userRepository.save(user);

        UserResponse userResponse = modelMapper.map(user, UserResponse.class);
        return userResponse;
    }

    public List<UserResponse> searchAll(String query) {
        List<User> users = userRepository.searchAll(query);
        List<UserResponse> userResponseList = users.stream().map(user -> modelMapper.map(user, UserResponse.class)).collect(Collectors.toList());
        return userResponseList;
    }
}
