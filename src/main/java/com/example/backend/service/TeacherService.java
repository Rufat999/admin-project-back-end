package com.example.backend.service;

import com.example.backend.dto.TeacherRequest;
import com.example.backend.dto.TeacherResponse;
import com.example.backend.exception.TeacherNotExistException;
import com.example.backend.model.Teacher;
import com.example.backend.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TeacherService {

    private final TeacherRepository teacherRepository;
    private final ModelMapper modelMapper;

    public List<TeacherResponse> getAllTeachers() {
        List<Teacher> teacherList = teacherRepository.findAll();

        List<TeacherResponse> teacherResponseList = teacherList.stream().map(teacher -> modelMapper.map(teacher, TeacherResponse.class)).collect(Collectors.toList());
        return teacherResponseList;
    }

    public TeacherResponse add(TeacherRequest request) {
        Teacher teacher = modelMapper.map(request, Teacher.class);

        teacherRepository.save(teacher);

        TeacherResponse teacherResponse = modelMapper.map(teacher, TeacherResponse.class);

        return teacherResponse;
    }

    public TeacherResponse getById(Long teacherId) {
        Teacher teacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new TeacherNotExistException("Teacher " + teacherId + " not found!"));
        TeacherResponse teacherResponse = modelMapper.map(teacher, TeacherResponse.class);
        return teacherResponse;
    }

    public TeacherResponse update(Long teacherId, TeacherRequest request) {
        Teacher teacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new TeacherNotExistException("Teacher " + teacherId + " not found!"));
        teacher.setTeacher(request.getTeacher());
        teacherRepository.save(teacher);
        TeacherResponse teacherResponse = modelMapper.map(teacher, TeacherResponse.class);
        return teacherResponse;
    }

    public void deleteById(Long teacherId) {
        teacherRepository.deleteById(teacherId);
    }

    public void deleteAll() {
        teacherRepository.deleteAll();
    }
}
