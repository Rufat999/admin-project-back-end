package com.example.backend.controller;

import com.example.backend.dto.TeacherRequest;
import com.example.backend.dto.TeacherResponse;
import com.example.backend.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
@RequiredArgsConstructor
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping("/all")
    public List<TeacherResponse> getAllTeachers(){
        return teacherService.getAllTeachers();
    }

    @GetMapping("/{teacherId}")
    public TeacherResponse getById(@PathVariable Long teacherId){
        return teacherService.getById(teacherId);
    }

    @PostMapping("/add")
    public TeacherResponse add(@RequestBody TeacherRequest request){
        return teacherService.add(request);
    }

    @PutMapping("/update/{teacherId}")
    public TeacherResponse update(@PathVariable Long teacherId, @RequestBody TeacherRequest request){
        return teacherService.update(teacherId, request);
    }

    @DeleteMapping("/delete/{teacherId}")
    public void deleteById(@PathVariable Long teacherId){
        teacherService.deleteById(teacherId);
    }

    @DeleteMapping("/delete/all")
    public void deleteAll(){
        teacherService.deleteAll();
    }
}
