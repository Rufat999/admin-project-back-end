package com.example.backend.controller;

import com.example.backend.dto.CommentRequest;
import com.example.backend.dto.CommentResponse;
import com.example.backend.dto.TeacherRequest;
import com.example.backend.service.CommentService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
@RequiredArgsConstructor
@RequestMapping("/comment")
public class CommentController {

    private final CommentService commentService;

    @GetMapping("/all")
    public List<CommentResponse> getAllComments(){
        return commentService.getAllComments();
    }

    @PostMapping("/{teacherId}/add")
    public CommentResponse add(@PathVariable Long teacherId, @RequestBody CommentRequest request){
        return commentService.add(teacherId, request);
    }

    @PutMapping("/update/{commentId}")
    public CommentResponse update(@PathVariable Long commentId, @RequestBody CommentRequest request){
        return commentService.update(commentId, request);
    }

    @DeleteMapping("/delete/{commentId}")
    public void deleteById(@PathVariable Long commentId){
        commentService.deleteById(commentId);
    }

    @DeleteMapping("/delete/all")
    public void deleteAll(){
        commentService.deleteAll();
    }
}
