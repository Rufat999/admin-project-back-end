package com.example.backend.controller;

import com.example.backend.dto.*;
import com.example.backend.model.User;
import com.example.backend.repository.UserRepository;
import com.example.backend.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/imagedata";

    private final ModelMapper modelMapper;
    private final UserService userService;
    private final UserRepository userRepository;

    @GetMapping("/{userId}")
    public UserResponse getUserById(@PathVariable Long userId) {
        return userService.getUserById(userId);
    }

    @GetMapping("/all")
    public List<UserResponse> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("/register")
    public UserResponse registerUser(@RequestBody UserRequest request) {
        return userService.registerUser(request);
    }

    @PostMapping("/login")
    public String login(@RequestBody UserLoginRequest request) {
        return userService.login(request);
    }

    @PutMapping("/{userId}/update")
    public UserResponse update(@PathVariable Long userId, @RequestBody UserRequest request) {
        return userService.update(userId, request);
    }

    @PutMapping("/resetPassword")
    public UserResponse resetPassword(@RequestBody UserResetPasswordRequest request) {
        return userService.resetPassword(request);
    }

    @DeleteMapping("/{userId}")
    public void deleteUserById(@PathVariable Long userId) {
        userService.deleteUserById(userId);
    }

    @DeleteMapping("/all")
    public void deleteAllUsers() {
        userService.deleteAllUsers();
    }

    @GetMapping("/search/all")
    public List<UserResponse> searchAll(@RequestParam("query") String query) {
        return userService.searchAll(query);
    }
}